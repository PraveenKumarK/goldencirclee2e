var LoginPageObjects = function() {
    var basepage=require("T:\\Goldencircle\\tests\\e2e\\pages\\basepage.po.js");
    this.get = function() {
        browser.driver.manage().window().maximize();
        browser.get('http://www.goldencircle.in/login');
        
    };
  /* Elements in Login Page */
   var UserName=element(by.css("[formControlName=\"userName\"]"));
   var Password=element(by.css("[formControlName=\"password\"]"));
   var WelcomeText=element(by.className("row welcome-rewards rs-md-welcome rs-sm-wlcm lg-wlcm-label"));
   var SignUpText=element(by.id("signup-title"));
   var error_alert=element(by.className("alert-error"));
   var FooterText=element(by.className( "col-lg-12 text-right af-copy"));
   var mainLogo=element(by.css("#login-component-container > div > div > div.row.program-logo > img"));
   var ForgotPasswordLink=element(by.className("lg-fp-label rs-sm-forget"));
   var SubmitBtn=element(by.buttonText('SUBMIT'));
   var signupbtn=element(by.buttonText('SIGN UP'));
   var UserNameFP=element(by.css("div.cdk-focus-trap-content > forgot-password > div > div:nth-child(2) > form > div > div > div.col-md-6.col-lg-8.col-sm-12.col-xs-12 > input"));


    this.ClickForgotPassword=function()
    {
        this.WaitForElement(ForgotPasswordLink);
        this.ClickElement(ForgotPasswordLink);
    }

    this.ClickSubmit=function()
    {
        this.WaitForElement(signupbtn);
        signupbtn.click();
    }

     this.ClickSignUp=function()
    {
        this.WaitForElement(SubmitBtn);
        SubmitBtn.click();
    }

    this.SUbmitButtonSate=function()
    {
        expect(SubmitBtn.isEnabled()).toBeFalsy();
  
    }
    this.CheckLogo=function()
        {
     expect(mainLogo.isPresent()).toBeTruthy();
     expect(mainLogo.getAttribute("src")).toEqual("http://www.goldencircle.in/assets/images/signup-annectos-logo1.png");
    
    }
   this.getErrortext=function()
   {
     return  this.getTextFromElement(error_alert);
   
   }
    this.getFootertext=function()
   {
     return  this.getTextFromElement(FooterText);
      }
   this.getWelcomeText=function()
   {
     return  this.getTextFromElement(WelcomeText);
   }

   this.getTextFromElement=function(e)
   {
   this.WaitForElement(e)
       return e.getText();
       
   }

   this.getSignUptext=function()
   {
       this.WaitForElement(SignUpText)
       return SignUpText.getText();
   }
    this.setPassword = function(value) {
         
                 
        this.WaitForElement(Password);
        Password.clear();
        Password.sendKeys(value);
        
    };

    this.WaitForElement=function(e)
    {
        var EC = protractor.ExpectedConditions;
        browser.wait(EC.elementToBeClickable(e),3000)
    }

     this.ClickElement=function(e)
    {
      e.click();
    }

    UserNameFP
     this.SetUserNameFP=function(value)
    {
         this.WaitForElement( UserNameFP);
         UserNameFP.clear();
         UserNameFP.sendKeys(value);
    }

    this.SetUserName=function(value)
    {
         this.WaitForElement( UserName);
         UserName.clear();
         UserName.sendKeys(value);
    }

    this.shouldHavePassword = function(value) {
        expect(this.Password.getAttribute('value')).toEqual(value);
    };

    this.PasswordShouldBeVisible = function() {
        expect(this.Password.isDisplayed()).toBeTruthy();
    };

    this.PasswordShouldNotBeVisible = function() {
        expect(this.Password.isDisplayed()).toBeFalsy();
    };

    this.PasswordShouldBeEnabled = function() {
        expect(this.Password.isEnabled()).toBeTruthy();
    };

    this.PasswordShouldNotBeEnabled = function() {
        expect(this.Password.isEnabled()).toBeFalsy();
    };

    this.lOGINButton = element(by.buttonText('LOGIN'));

    this.clickLOGINButton = function() {
        this.lOGINButton.click();
    };

    this.lOGINButtonShouldBeVisible = function() {
        expect(this.lOGINButton.isDisplayed()).toBeTruthy();
    };

    this.lOGINButtonShouldNotBeVisible = function() {
        expect(this.lOGINButton.isDisplayed()).toBeFalsy();
    };

    this.lOGINButtonShouldHaveClass = function(className) {
        this.lOGINButton.getAttribute('class').then(function(classes) {
            expect(classes.split(' ').indexOf(className) !== -1).toBeTruthy();
        });
    };

    this.lOGINButtonShouldNotHaveClass = function(className) {
        this.lOGINButton.getAttribute('class').then(function(classes) {
            expect(classes.split(' ').indexOf(className) === -1).toBeTruthy();
        });
    };

    this.sIGNUPButton = element(by.buttonText('SIGN UP'));

    this.clickSIGNUPButton = function() {
        this.sIGNUPButton.click();
    };

    this.sIGNUPButtonShouldBeVisible = function() {
        expect(this.sIGNUPButton.isDisplayed()).toBeTruthy();
    };

    this.sIGNUPButtonShouldNotBeVisible = function() {
        expect(this.sIGNUPButton.isDisplayed()).toBeFalsy();
    };

    this.sIGNUPButtonShouldHaveClass = function(className) {
        this.sIGNUPButton.getAttribute('class').then(function(classes) {
            expect(classes.split(' ').indexOf(className) !== -1).toBeTruthy();
        });
    };

    this.sIGNUPButtonShouldNotHaveClass = function(className) {
        this.sIGNUPButton.getAttribute('class').then(function(classes) {
            expect(classes.split(' ').indexOf(className) === -1).toBeTruthy();
        });
    };

};
module.exports = new LoginPageObjects();