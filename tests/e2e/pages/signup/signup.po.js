var SignUpPageObjects=function()
{
    var basepage=require("../basepage.po.js");

    this.get = function() {
        browser.driver.manage().window().maximize();
        browser.get('http://www.goldencircle.in/signup');
        
    };
    
    var signUpText=element(by.id("titleSignUp"));
    
    this.getWelcomeTextfromSignUpPage=function()
    {
        return this.text1=basepage.getTextFromElement(signUpText);
    }
    this.SignUpTextVisible=function()
    {
          expect(signUpText.isDisplayed()).toBeTruthy();
          expect(basepage.getTextFromElement(signUpText)).toEqual('Sign up for our Rewards Programs');
    } 
};
module.exports = new SignUpPageObjects();